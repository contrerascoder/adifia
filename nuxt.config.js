module.exports = {
    ssr: false,
    srcDir: 'app/',
    modules: [
        // Simple usage
        'nuxt-buefy',
    ],
    plugins: [
        {src: '~/plugins/persistedstate.js', ssr: false}
    ],
    head: {
        titleTemplate: 'Adifia | %s',
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' }
        ],
        link: [
            {rel: 'stylesheet', href: 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css'}
        ]
    }
}