const mongoose = require('mongoose')
const { URIDB } = require('../config/vars')

/**
 * @type {mongoose.ConnectOptions}
 */
const connectOptions = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
}

module.exports = function connectDB(cb) {
    mongoose.connect(URIDB, connectOptions, (err) => {
        if (err) {
            console.log('Hubo un error conectandose a ' + URIDB)
            return process.exit(1)
        }
        cb()
    })

}