const connectdb = require("../utils/connectdb");
const Article = require('../models/Post').model
connectdb(async () =>{
    for (let index = 0; index < 80; index++) {
        const element = 'Articulo numero ' + index;
        const rndnumber = Math.random()
        const publishState = rndnumber > 0.3 ? 'published' : 'unpublished'

        await Article.create({
            title: element,
            body: `<p>Esto es el cuerpo del articulo</p>`,
            categories: '60427e555a85cb6a94e41754',
            ownerId: "60427b1a834b976603eca17e",
            tags: ['aaa', 'bbbb'],
            state: publishState,
        })

        console.log('articulo creado');
    }
})