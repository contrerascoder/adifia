const express = require('express')
const adminBroRouter = require('./adminbro/index')
const router = express.Router()

router.use('/admin', adminBroRouter)
router.use('/api', require('./api'))

module.exports = router