const AdminBro = require('admin-bro')
const AdminBroExpressjs = require('@admin-bro/express')
const User = require('../../models/User').model

AdminBro.registerAdapter(require('@admin-bro/mongoose'))

const adminBro = new AdminBro({
    resources: require('../../models/resources'),
    rootPath: '/admin'
})

module.exports = AdminBroExpressjs.buildAuthenticatedRouter(adminBro, {
    authenticate: async (email, password) => {
        const user = await User.findOne({ email })
        if (user) {
            const matched = true //await bcrypt.compare(password, user.password)
            if (matched) {
                return user
            }
        }
        return false
    },
    cookiePassword: 'some-secret-password-used-to-secure-cookie',
})