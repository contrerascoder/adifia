const express = require('express')
const fns = require('./functions')
const auth = require('./auth')
const { decodeToken } = require('./token')
const multer = require('./multer')

const router = express.Router()

router.get('/articles', fns.articles)
router.get('/nextevent', fns.nextEvent)
router.post('/upload', decodeToken, multer.any(), fns.uploadImage)

router.use('/auth', (function () {
    const router = express.Router()

    router.post('/register', auth.register)
    router.post('/login', auth.authenticate)
    router.get('/verify', decodeToken, (req, res) => {
        res.status(200).json(req.user)
    })

    return router
})())

module.exports = router