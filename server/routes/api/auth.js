const bcrypt = require('bcrypt')
const Photograph = require('../../models/Photograph').photographModel
const {validateNif} = require('@willowi/validate-nif')
const {createToken} = require('./token')
const {SALTS} = require('../../config/vars')

module.exports = {
    /**
     * 
     * @param {import('express').Request} req 
     * @param {import('express').Response} res 
     */
    async authenticate(req, res) {
        const user = await Photograph.findOne({email: req.body.email})
        if ( !user || !(await compare(req.body.password, user.password)) ) {
            return res.status(404).end("Las credenciales no son correctas")
        }
        return res.status(201).json({
            token: createToken(user),
            userJson: JSON.stringify({email: user.email, nif: user.nif})
        })
    },

    /**
     * 
     * @param {import('express').Request} req 
     * @param {import('express').Response} res 
     */
    async register(req, res) {
        const year = new Date().getFullYear()
        if (validateNif(req.body.nif) != 1) {
            return res.status(400).end("El dni no es valido")
        }
        if (await Photograph.findOne({ip: req.ip, year})) {
            return res.status(403).end("Este año ya te has registrado")
        }
        await Photograph.create({
            ...req.body,
            ip: req.ip,
            password: await hash(req.body.password),
            year: year
        });
        return res.status(201).end("Ya te has registrado, recuerda que solo puedes subir 3 fotos")
    }
}

async function hash(password) {
    return await bcrypt.hash(password, SALTS)
}

async function compare(password, hash) {
    return await bcrypt.compare(password, hash)
}