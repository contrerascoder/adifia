const jwt = require('jsonwebtoken')
const Photographer = require('../../models/Photograph').photographModel
const {SECRET, TOKEN_EXPIRATION} = require('../../config/vars')

function createToken({email, nif}) {
    return jwt.sign({email, nif}, SECRET, {expiresIn: TOKEN_EXPIRATION})
}

async function decodeToken(req, res, next) {
    try {
        const token = req.headers.authorization
        const verified = jwt.verify(token, SECRET)
        req.user = await Photographer.findOne({email: verified.email})
        next()
    } catch (error) {
        if (error.message == 'jwt expired') {
            return res.status(404).end('Tienes que vovlerte a loguear')
        } else {
            console.log(error);
            return res.status(500).end('Error interno')
        }
    }
}

module.exports = {createToken, decodeToken}