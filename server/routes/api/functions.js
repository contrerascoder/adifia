const Article = require('../../models/Post').model
const Event = require('../../models/Event').events
const Photo = require('../../models/Photo').photo
const uploadCloudinary = require('./uploadimages')

const pageSize = 10

module.exports = {
    /**
     * 
     * @param {import('express').Request} req 
     * @param {import('express').Response} res 
     */
    async articles(req, res) {
        const articles = await Article.find({state: 'published'})
            .sort('-createdAt')
            .skip(((req.query.page || 1) - 1) * pageSize)
            .limit(pageSize)
        
        const totalItems = await Article.find({state: 'published'}).count()
        res.status(200).json({items: articles, totalItems: totalItems})
    },

    /**
     * 
     * @param {import('express').Request} req 
     * @param {import('express').Response} res 
     */
    async nextEvent(req, res) {
        const nextevent = await Event.findOne({state: 'published', date: {$gt: Date.now()}}).sort('date')

        res.status(200).json(nextevent)
    },

    /**
     * 
     * @param {import('express').Request} req 
     * @param {import('express').Response} res 
     */
    async uploadImage(req, res) {
        console.log(req.user);
        console.log(req.body.title)
        const result = await uploadCloudinary(req.files[0].path)
        console.log(result.url); // direccion de imagen a guardar en base de datos
        await Photo.create({
            ...req.body,
            photoFilePath: result.url,
            photograph: req.user
        })
        res.end('La foto se ha subido correctamente')
    }
}