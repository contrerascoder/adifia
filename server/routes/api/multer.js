const multer = require('multer')
const path = require('path')
const uploadPath = path.resolve(__dirname, '../../../fotodenuncia/photos/')
console.log(uploadPath);

module.exports = multer({
    storage: multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, uploadPath)
        },
        filename: function (req, file, cb) {
            cb(null, file.fieldname + '-' + Date.now() + '.png')
        }
    })
})