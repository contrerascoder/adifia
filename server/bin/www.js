const http = require('http')
const { PORT } = require('../config/vars')
const connectDB = require('../utils/connectdb')

connectDB(() => {
    const server = http.createServer(require('../config/app'))
    
    server.listen(PORT, () => console.log('App escuchando por el puerto ' + PORT))
})