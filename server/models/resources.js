

module.exports = [
    require('./User').resource,
    require('./Post').resource,
    require('./Event').resource,
    require('./Category').resource,
    require('./Role').resource,
    require('./Photograph').resource,
    require('./Photo').resource,
]