const mongoose = require('mongoose')

const UserSchema = new mongoose.Schema({
    name: {type: String,required: true},
    surname: {type: String,required: true},
    email: {type: String,required: true},
    password: {type: String,required: true},
    roles: [{type: mongoose.Types.ObjectId, ref: 'role', required: true}]
})

const User = mongoose.model('user', UserSchema)

const resource = {
    resource: User,
    options: {
        properties: {
            encryptedPassword: {
                isVisible: false,
            },
            password: {
                type: 'string',
                isVisible: {
                    list: false, edit: true, filter: false, show: false,
                },
            },
        },
        actions: {
            new: {
                async before(request) {
                    console.log(request);
                    if (request.payload.password) {
                        request.payload = {
                            ...request.payload,
                            password: await bcrypt.hash(request.payload.password, SALTS)
                        }
                    }
                    return request
                },
            }
        }
    }
}

module.exports = {resource, model: User}
