const mongoose = require('mongoose')

const PhotoSchema = new mongoose.Schema({
    title: {type: String, required: true},
    description: {type: String, required: true},
    photoFilePath: {type: String, required: true},
    photograph: {type: mongoose.Types.ObjectId, ref: 'photograph'}
})

const photo = mongoose.model('photo', PhotoSchema)
const resource = {
    resource: photo
}


module.exports = {resource, photo}