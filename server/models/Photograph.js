const mongoose = require('mongoose')

const PhotographSchema = new mongoose.Schema({
    name: {type: String, required: true},
    surname: {type: String, required: true},
    email: {type: String, required: true},
    nif: {type: String, required: true},
    ip: {type: String, required: true},
    password: {type: String, required: true},
    year: {type: Number, default: () => new Date().getFullYear()}
})

const photograph = mongoose.model('photograph', PhotographSchema)
const resource = {
    resource: photograph
}


module.exports = {resource, photographModel: photograph}