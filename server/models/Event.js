const mongoose = require('mongoose')

const eventSchema = new mongoose.Schema({
    title: {type: String, required: true},
    description: {type: String, required: true},
    date: {type: Number, default: Date.now},
    state: {type: String, enum: ['published', 'unpublished']}
})

const event = mongoose.model('events', eventSchema)

const resource = {
    resource: event,
    options: {
        properties: {
            date: {
                type: 'date'
            }
        },
        actions: {
            new: {
                async before(request) {
                    console.log(request);
                    request.payload = {
                        ...request.payload,
                        date: new Date(request.payload.date).getTime()
                    }
                    console.log(request);
                    return request
                },
            }
        }
    },
}


module.exports = {resource, events: event}