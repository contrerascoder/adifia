const mongoose = require('mongoose')

const PostSchema = new mongoose.Schema({
    title: {type: String, required: true},
    body: {type: String, required: true},
    ownerId: {type: mongoose.Types.ObjectId, ref: 'user'},
    categories: {type: mongoose.Types.ObjectId, ref: 'category', required: true},
    tags: [{type: String, required: true}],
    state: {type: String, enum: ['published', 'unpublished'], default: 'unpublished'},
    createdAt: {type: Number, default: Date.now}
})

const post = mongoose.model('post', PostSchema)

const resource = {
    resource: post,
    options: {
        properties: {
            body: {
                type: 'richtext'
            }
        }
    }
}


module.exports = {resource, model: post}