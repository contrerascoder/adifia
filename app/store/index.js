export const state = () => ({
    token: '',
    userJson: ''
})

export const mutations = {
    loginInfo(state, info) {
        debugger
        state.token = info.token
        state.userJson = info.userJson
    },
    logout(state, info) {
        state.token = ""
        state.userJson = ""
    }
}

export const getters = {
    userInfo(state) {
        return JSON.parse(state.userJson)
    }
}