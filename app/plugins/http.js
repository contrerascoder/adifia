import axios from 'axios'

export default axios.create({
    baseURL: 'https://adifiadminbro.herokuapp.com/api'
})