import createPersistedState from 'vuex-persistedstate'
import http from './http'
export default ({store}) => {
    console.log('creating persisted state');
  createPersistedState({
      key: 'adifia',
      // paths: [...]
  })(store)
  if (store.state.token)
    http.defaults.headers.authorization = store.state.token
}